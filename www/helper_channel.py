# BlueJay - RIM BlackBerry Browser Exploit/Tool
# Copyright (C) 2011-2012 Core Security Technologies
# Released by Federico Muttis (@acid_) at RSA Conference 2013
# acid(at)coresecurity.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Channel handler for communications between the console and the javascript
# that runs on the BlackBerry Browser.

import time
import struct

class web_page:
    def __init__(self, request_method, params, communicator):
        self.__params = dict()
        self.communicator = communicator
        if len(params[0]) > 1:
            self.__params = dict(params)
    
    def render(self):
        ret = ""
        if self.communicator.restart_browser_step1:
            ret = "relaunch"
            self.communicator.restart_browser_step1 = False
            self.communicator.restart_browser_step2 = True
            
        return ret
