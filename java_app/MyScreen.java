// BlueJay Helper - RIM BlackBerry Browser Exploit/Tool Companion
// Copyright (C) 2011-2012 Core Security Technologies
// Released by Federico Muttis (@acid_) at RSA Conference 2013
// acid(at)coresecurity.com
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package mypackage;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import net.rim.device.api.io.transport.ConnectionFactory;
import net.rim.device.api.system.Backlight;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.blackberry.api.browser.Browser;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.ApplicationManager;
import net.rim.device.api.system.CodeModuleManager;
import net.rim.device.api.system.CodeModuleGroup;
import java.lang.Thread;
import net.rim.device.api.system.Device;


/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class MyScreen extends MainScreen implements FieldChangeListener
{
	private ButtonField _startBtn;
    private BasicEditField _editIP;
    private boolean _urlConfirmed = false;
    private boolean _keepLaunching = false;
    
	public final class insTask extends TimerTask {
		public void run() {
			// Insomnia is important to dump long regions..
			Backlight.setTimeout(255);
			Backlight.enable(true);
			
			// Check if we have to relaunch the browser..
			if (_urlConfirmed) {
				String inspector_url = _editIP.getText();
		        HttpConnection connection = null;
		        inspector_url = inspector_url + "/helper_channel.py";
		        //use API 5.0 Connection factory class to get first available connection
		        byte responseData[] = null;
		        try { 
		            connection = (HttpConnection) new ConnectionFactory().getConnection(inspector_url).getConnection();
		            int len = (int) connection.getLength();
		            responseData = new byte[len];
		            DataInputStream dis;
		            dis = new DataInputStream(connection.openInputStream());
		            dis.readFully(responseData);
		        } catch (Exception e) {
		             return;
		        } 
		        String resp = new String(responseData);
		        if ((resp.startsWith("relaunch")) || (_keepLaunching))  {
					launch_browser();
				}
			}
		}	
	}
    
    public MyScreen()
    {
    	setTitle("BlueJay Helper");
    	_editIP = new BasicEditField("Enter IP: ", "http://192.168.0.1", 30, EditField.FILTER_URL | EditField.FIELD_VCENTER );
    	
    	_startBtn = new ButtonField("Ok", Field.FIELD_HCENTER | Field.FIELD_VCENTER | ButtonField.NEVER_DIRTY);
    	_startBtn.setChangeListener(this);
    	
    	VerticalFieldManager vfm = new VerticalFieldManager(Field.FIELD_VCENTER);
       	add(_editIP);
    	add(_startBtn);
    	
		final Timer insomniaTimer = new Timer();
		UiApplication.getUiApplication().invokeLater( new Runnable() { public void run() { insomniaTimer.schedule(new insTask(), 0, 30000); } } );
    }
    
    public void launch_browser() {
    	String inspector_url = _editIP.getText();    
    	if (_keepLaunching) {
    		_keepLaunching = false;
    		Browser.getDefaultSession().displayPage(inspector_url);
    	} else {
    		_keepLaunching = true;
    		Browser.getDefaultSession().displayPage(inspector_url + "/mem_read.py?address=4294967295&size=32"); // 0xffffffff read of size 32, browser should crash.
    	}
    }

    public void reboot_device() {
    	net.rim.device.api.system.Device.requestPowerOff( true);
    }

    
    public void fieldChanged(Field field, int context)
    {
    	if (field == _startBtn) {
    		_urlConfirmed = true;
    		launch_browser();
    	}
    }   
}    
